<?php

/**
 * @see https://pastpapers.papacambridge.com/papers/caie/cambridge-upper-secondary-gce-international-o-level
 * @see https://pastpapers.papacambridge.com/papers/caie/cambridge-upper-secondary-igcse
 */
$exams = [
    'gce' => [
        'mathematics-d-4024'          => 'https://pastpapers.papacambridge.com/papers/caie/cambridge-upper-secondary-gce-international-o-level-mathematics-d-calculator-version-4024-',
        'mathematics-additional-4037' => 'https://pastpapers.papacambridge.com/papers/caie/cambridge-upper-secondary-gce-international-o-level-mathematics-additional-4037-',
        'french-3015'                 => 'https://pastpapers.papacambridge.com/papers/caie/cambridge-upper-secondary-gce-international-o-level-french-3015-',
        'english-language-1123'       => 'https://pastpapers.papacambridge.com/papers/caie/cambridge-upper-secondary-gce-international-o-level-english-language-1123-',
        'design-and-technology-6043'  => 'https://pastpapers.papacambridge.com/papers/caie/cambridge-upper-secondary-gce-international-o-level-design-and-technology-6043-',
        'physics-5054'                => 'https://pastpapers.papacambridge.com/papers/caie/cambridge-upper-secondary-gce-international-o-level-physics-5054-',
        'computer-science-2210'       => 'https://pastpapers.papacambridge.com/papers/caie/cambridge-upper-secondary-gce-international-o-level-computer-science-2210',
    ],
    'gcse' => [
        'french-8129'                => '',
        'french-9094'                => '',
        'general-paper-8019'         => '',
        'design-and-technology-9705' => 'https://pastpapers.papacambridge.com/papers/caie/cambridge-advanced-as-and-a-level-design-and-technology-9705',
        'mathematics-9709'           => 'https://pastpapers.papacambridge.com/papers/caie/cambridge-advanced-as-and-a-level-mathematics-9709',
        'physics-9702'               => 'https://pastpapers.papacambridge.com/papers/caie/cambridge-advanced-as-and-a-level-physics-9702',
        'computer-science-9608'      => 'https://pastpapers.papacambridge.com/papers/caie/cambridge-advanced-as-and-a-level-computer-science-9608',
    ],
];
