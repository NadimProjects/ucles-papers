<?php

/**
 * @param $url
 * @param $href
 * @return string
 */
function resolveUrl($url, $href): string
{
    $parseUrl = parse_url($url);
    $paths    = explode('/', $parseUrl['path']);
    array_pop($paths);
    $paths[]  = $href;

    return $parseUrl['scheme'] . '://' . $parseUrl['host'] . implode('/', $paths);
}
