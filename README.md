# README #

This script downloads exams papers from PapaCambridge.com

### How do I get set up? ###

1. Make a copy of `papers-links-DIST.php`

2. Name the new file `papers-links.php`

3. Edit the new file to add any new URLs

4. Run the script: `$ php ./index.php`
