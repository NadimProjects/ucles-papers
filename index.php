<?php

if ((@include __DIR__ . DIRECTORY_SEPARATOR . 'papers-links.php') === FALSE) {
    die('File "papers-links.php" does not exist.' . PHP_EOL);
}

const PAPER_FOLDER = __DIR__ . DIRECTORY_SEPARATOR . 'papers';

include_once 'simple_html_dom/simple_html_dom.php';
include_once 'utils/slugify.php';
include_once 'utils/url.php';

set_time_limit(0);

echo PHP_EOL . 'DOWNLOADING PAPERS ...' . PHP_EOL . PHP_EOL;

/**
 * @var array  $exams
 * @var string $level
 * @var array  $subjects
 */
foreach ($exams as $level => $subjects) {
    echo strtoupper($level) . PHP_EOL;
    foreach ($subjects as $subject => $url) {
        $folder = PAPER_FOLDER
            . DIRECTORY_SEPARATOR
            . $level
            . DIRECTORY_SEPARATOR
            . slugify($subject)
        ;
        if (!file_exists($folder) && !mkdir($folder, 0777, true) && !is_dir($folder)) {
            break;
        }

        echo ' - ' . $subject . PHP_EOL;

        if (!empty($url)) {
            $html = file_get_html($url);
            foreach($html->find('td a') as $el) {
                $period = slugify(html_entity_decode($el->plaintext));

                $pastPapersURL = resolveUrl($url, $el->href);
                $html = file_get_html($pastPapersURL);
                foreach($html->find('td div a') as $pdfHREF) {
                    $href   = $pdfHREF->href;
                    $pdfURL = resolveUrl($pastPapersURL, $href);
                    if (str_ends_with($pdfURL, '.pdf')) {

                        $paper = $period . '--' . basename($pdfURL);
                        echo '   - ' . $paper . str_repeat('.', (50 - mb_strlen($paper)));

                        $filename = $folder . DIRECTORY_SEPARATOR . $paper;
                        if (file_exists($filename)) {
                            echo ' ☒' . PHP_EOL;
                            break;
                        }

                        ob_start();
                        echo ' ☑' . PHP_EOL;

                        $pdfContents = file_get_contents($pdfURL);
                        $h = fopen($filename, 'wb');
                        fwrite($h, $pdfContents);
                        fclose($h);

                        echo ob_get_clean();
                    }
                }
            }
        }
    }
}

echo PHP_EOL . 'DONE' . PHP_EOL;
